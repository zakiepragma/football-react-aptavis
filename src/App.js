import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Club from "./pages/Club";
import Match from "./pages/Match";
import Standing from "./pages/Standing";

const App = () => {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Club />} />
        <Route path="/match" element={<Match />} />
        <Route path="/standing" element={<Standing />} />
      </Routes>
    </Router>
  );
};

export default App;
