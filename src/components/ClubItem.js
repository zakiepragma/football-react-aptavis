import React from "react";

const ClubItem = ({ index, club, onDelete, onEdit }) => {
  return (
    <tr>
      <td className="border px-4 py-2">{index + 1}</td>
      <td className="border px-4 py-2">{club.name}</td>
      <td className="border px-4 py-2">{club.city}</td>
      <td className="border px-4 py-2">
        <button
          className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded mr-2"
          onClick={() => onEdit(club)}
        >
          Edit
        </button>
        <button
          className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
          onClick={() => onDelete(club.id)}
        >
          Delete
        </button>
      </td>
    </tr>
  );
};

export default ClubItem;
