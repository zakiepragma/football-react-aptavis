import axios from "axios";
import React, { useEffect, useState } from "react";
import ClubItem from "../components/ClubItem";

const Club = () => {
  const [name, setName] = useState("");
  const [city, setCity] = useState("");
  const [loading, setLoading] = useState("");
  const [clubs, setClubs] = useState([]);
  const [buttonName, setButtonName] = useState("Add");
  const [clubId, setClubId] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      alert("Name is required!");
      return;
    }
    if (city.trim() === "") {
      alert("City is required!");
      return;
    }
    axios
      .post("http://localhost:8000/api/club/", {
        name,
        city,
      })
      .then((response) => {
        getClubs();
        setName("");
        setCity("");
      })
      .catch((error) => {
        alert(error.response.data.message);
        console.log(error);
      });
  };

  const handleUpdate = (e) => {
    e.preventDefault();
    if (name.trim() === "") {
      alert("Name is required!");
      return;
    }
    if (city.trim() === "") {
      alert("City is required!");
      return;
    }
    axios
      .put(`http://localhost:8000/api/club/${clubId}`, {
        name,
        city,
      })
      .then((response) => {
        getClubs();
        setName("");
        setCity("");
        setButtonName("Add");
      })
      .catch((error) => {
        alert(error.response.data.message);
        console.log(error);
      });
  };

  const getClubs = async () => {
    await axios.get("http://localhost:8000/api/club/").then((response) => {
      setLoading(false);
      setClubs(response.data);
      console.log(response.data);
    });
  };

  function deleteClub(id) {
    const confirmed = window.confirm(
      "Apakah Anda yakin ingin menghapus data ini?"
    );
    if (confirmed) {
      axios
        .delete(`http://localhost:8000/api/club/${id}`)
        .then((response) => {
          getClubs();
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }

  function editClub(club) {
    setButtonName("Update");

    setName(club.name);
    setCity(club.city);
    setClubId(club.id);
  }

  useEffect(() => {
    setLoading(true);
    getClubs();
  }, []);

  return (
    <div className="p-10">
      <form
        className="flex items-center justify-center gap-5 mb-10"
        onSubmit={buttonName === "Add" ? handleSubmit : handleUpdate}
      >
        <div className="mb-4">
          <label className="block text-gray-700 font-bold mb-2" htmlFor="name">
            Name
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="name"
            type="text"
            placeholder="Enter club name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className="mb-4">
          <label className="block text-gray-700 font-bold mb-2" htmlFor="city">
            City
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="city"
            type="text"
            placeholder="Enter city"
            value={city}
            onChange={(e) => setCity(e.target.value)}
          />
        </div>
        <div className="mt-3">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            {buttonName}
          </button>
        </div>
      </form>
      <table className="text-center table-auto w-full">
        <thead>
          <tr>
            <th className="px-4 py-2">#</th>
            <th className="px-4 py-2">Name</th>
            <th className="px-4 py-2">City</th>
            <th className="px-4 py-2">Actions</th>
          </tr>
        </thead>
        <tbody>
          {clubs.length <= 0 ? (
            <tr className="">
              <td colSpan={3}>
                <p className="text-yellow-900 underline italic mt-5">
                  Tidak ada data
                </p>
              </td>
            </tr>
          ) : (
            <>
              {clubs.map((club, index) => (
                <ClubItem
                  index={index}
                  key={club.id}
                  club={club}
                  onDelete={deleteClub}
                  onEdit={editClub}
                />
              ))}
            </>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Club;
