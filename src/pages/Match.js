import axios from "axios";
import React, { useEffect, useState } from "react";

const Match = () => {
  const [clubs, setClubs] = useState([]);

  const [formData, setFormData] = useState({
    home_team_id: "",
    away_team_id: "",
    home_team_score: 0,
    away_team_score: 0,
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(formData);
    // lakukan request API untuk menyimpan data ke server
    if (formData.home_team_id === "") {
      alert("Home Team is selected!");
      return;
    }
    if (formData.home_team_score === "") {
      alert("Home Team Score is required!");
      return;
    }
    if (formData.away_team_id === "") {
      alert("Away Team is selected!");
      return;
    }
    if (formData.away_team_score === "") {
      alert("Away Team Score is required!");
      return;
    }
    axios
      .post("http://localhost:8000/api/match-score/", formData)
      .then((response) => {
        if (formData.home_team_score > formData.away_team_score) {
          alert("Congratulation home team");
        } else if (formData.home_team_score < formData.away_team_score) {
          alert("Congratulation away team");
        } else {
          alert("Draw, a satisfying result for both teams");
        }
      })
      .catch((error) => {
        alert(error.response.data.error);
        console.log(error);
      });
  };

  const getClubs = async () => {
    await axios.get("http://localhost:8000/api/club/").then((response) => {
      setClubs(response.data);
      console.log(response.data);
    });
  };

  useEffect(() => {
    getClubs();
  }, []);

  function generateRandomNumber(min, max) {
    let score1 = Math.floor(Math.random() * (max - min + 1) + min);
    let score2 = Math.floor(Math.random() * (max - min + 1) + min);

    let team1 = Math.floor(Math.random() * clubs.length + 1);
    let team2 = Math.floor(Math.random() * clubs.length + 1);

    while (team1 === team2) {
      team2 = Math.floor(Math.random() * clubs.length + 1);
    }

    setFormData({
      home_team_id: team1,
      away_team_id: team2,
      home_team_score: score1,
      away_team_score: score2,
    });
  }

  function deleteRandom() {
    setFormData({
      home_team_id: "",
      away_team_id: "",
      home_team_score: 0,
      away_team_score: 0,
    });
  }

  return (
    <div className="p-10">
      <div className="text-center mb-10">
        <p className="text-4xl text-bold">EUROPEAN SUPER LEAGUE</p>
        <p className="text-1xl text-bold">SUPER BIG MATCH</p>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="flex flex-row justify-center items-center gap-6">
          <div className="flex flex-col">
            <label htmlFor="home_team_id" className="mb-2">
              Home Team
            </label>
            <select
              name="home_team_id"
              id="home_team_id"
              onChange={handleInputChange}
              value={formData.home_team_id}
              className="border border-gray-300 rounded-md px-4 py-2"
            >
              <option value="">-- Pilih Home Team --</option>
              {clubs.map((club) => (
                <option value={club.id} key={club.id}>
                  {club.name}
                </option>
              ))}
            </select>
          </div>

          <div className="flex flex-col">
            <div className="flex gap-4">
              <input
                type="number"
                name="home_team_score"
                id="home_team_score"
                onChange={handleInputChange}
                value={formData.home_team_score}
                min="0"
                className="border border-gray-300 rounded-md px-4 py-2 w-24 mt-8 text-center"
              />
              <span className="mt-10">VS</span>
              <input
                type="number"
                name="away_team_score"
                id="away_team_score"
                onChange={handleInputChange}
                value={formData.away_team_score}
                min="0"
                className="border border-gray-300 rounded-md px-4 py-2 w-24 mt-8 text-center"
              />
            </div>
          </div>

          <div className="flex flex-col">
            <label htmlFor="away_team_id" className="mb-2">
              Away Team
            </label>
            <select
              name="away_team_id"
              id="away_team_id"
              onChange={handleInputChange}
              value={formData.away_team_id}
              className="border border-gray-300 rounded-md px-4 py-2"
            >
              <option value="">-- Pilih Away Team --</option>
              {clubs.map((club) => (
                <option value={club.id} key={club.id}>
                  {club.name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="flex justify-center items-center mt-10">
          <button
            type="submit"
            className="bg-blue-500 text-white px-4 py-2 rounded-md"
          >
            Submit
          </button>
        </div>
      </form>
      <div className="flex justify-center items-center mt-10 gap-4">
        <button
          onClick={() => {
            deleteRandom();
          }}
          className="bg-yellow-500 text-white px-4 py-2 rounded-md"
        >
          Refresh
        </button>
        <button
          onClick={() => {
            generateRandomNumber(0, 7);
          }}
          className="bg-orange-500 text-white px-4 py-2 rounded-md"
        >
          Generate Match
        </button>
      </div>
    </div>
  );
};

export default Match;
