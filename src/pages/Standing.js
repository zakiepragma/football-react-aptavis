import { useState, useEffect } from "react";
import axios from "axios";

function Standing() {
  const [klasemen, setKlasemen] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8000/api/standing").then((response) => {
      setKlasemen(response.data);
      console.log(response);
    });
  }, []);

  return (
    <div className="p-10 max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
      <h1 className="text-2xl font-bold mb-4">
        Standing European Super League
      </h1>
      <table className="table-auto w-full">
        <thead>
          <tr>
            <th className="px-4 py-2">#</th>
            <th className="px-4 py-2">Klub</th>
            <th className="px-4 py-2">Main</th>
            <th className="px-4 py-2">Menang</th>
            <th className="px-4 py-2">Seri</th>
            <th className="px-4 py-2">Kalah</th>
            <th className="px-4 py-2">GM</th>
            <th className="px-4 py-2">GK</th>
            <th className="px-4 py-2">SG</th>
            <th className="px-4 py-2">Poin</th>
          </tr>
        </thead>
        <tbody>
          {klasemen.map((item, index) => (
            <tr key={index}>
              <td className="border px-4 py-2 text-center">{index + 1}</td>
              <td className="border px-4 py-2">{item.club}</td>
              <td className="border px-4 py-2 text-center">{item.ma}</td>
              <td className="border px-4 py-2 text-center">{item.me}</td>
              <td className="border px-4 py-2 text-center">{item.s}</td>
              <td className="border px-4 py-2 text-center">{item.k}</td>
              <td className="border px-4 py-2 text-center">{item.gm}</td>
              <td className="border px-4 py-2 text-center">{item.gk}</td>
              <td className="border px-4 py-2 text-center">
                {item.gm - item.gk}
              </td>
              <td className="border px-4 py-2 text-center">{item.point}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Standing;
